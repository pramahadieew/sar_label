class Report {
  // final int id;
  final String kode;
  final String kdLokasi;
  final String kdBrg;
  final String noAset;
  final String urBaru;
  final String tglPerlh;
  final String merkType;
  final String asalPerlh;
  final String rphSat;
  final String keterangan;
  final String noMesin;
  final String noRangka;
  final String noPolisi;
  final String noBpkb;

  Report(
      {
      // this.id,
      this.kode,
      this.kdLokasi,
      this.kdBrg,
      this.noAset,
      this.urBaru,
      this.tglPerlh,
      this.merkType,
      this.asalPerlh,
      this.rphSat,
      this.keterangan,
      this.noMesin,
      this.noRangka,
      this.noPolisi,
      this.noBpkb});

  factory Report.fromJson(Map<String, dynamic> json) {
    return new Report(
      // id: json['id'],
      kode: json['kode'],
      kdLokasi: json['kd_lokasi'],
      kdBrg: json['kd_brg'],
      noAset: json['no_aset'],
      urBaru: json['ur_baru'],
      tglPerlh: json['tgl_perlh'],
      merkType: json['merk_type'],
      asalPerlh: json['asal_perlh'],
      rphSat: json['rph_sat'],
      keterangan: json['keterangan'],
      noMesin: json['no_mesin'],
      noRangka: json['no_rangka'],
      noPolisi: json['no_polisi'],
      noBpkb: json['no_bpkb'],
    );
  }
}
