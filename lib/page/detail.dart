import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:sar_label/Report.dart';
import 'package:http/http.dart' as http;

class Detail extends StatefulWidget {
  final String code;
  // final String nup;
  Detail(this.code);
  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
  Report report;
  Future<Report> _getProduk() async {
    await http
        .get("http://192.168.43.87/restapi/getproduk.php?kode=${widget.code}")
        .then((response) {
      if (jsonDecode(response.body) != null) {
        setState(() {
          report = Report.fromJson(jsonDecode(response.body));
          // print(report.kdSatker);
        });
      }
    });
    return report;
  }

  @override
  void initState() {
    _getProduk();
    super.initState();
  }

  var ifs;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Hasil Scan"), backgroundColor: Colors.orange[300],
        // backgroundColor: Colors.orange[300],
      ),
      body: Center(
        child: Card(
          // elevation: 2,
          // margin: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
          child: report == null
              ? Text(
                  "Asset Tidak di Temukan, ${widget.code}",
                  style: TextStyle(fontSize: 20),
                )
              : Container(
                  child: Padding(
                    padding: EdgeInsets.all(16.0),

                    // child: Align(
                    //       alignment: Alignment.centerLeft,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Center(
                          child: Column(
                            children: <Widget>[
                              Padding(padding: EdgeInsets.all(16.0)),
                              Text(
                                "Detail Rincian Aset",
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(""),
                            ],
                          ),
                        ),
                        //       const ListTile(
                        //   leading: Icon(Icons.album),
                        //   title: Text('The Enchanted Nightingale'),
                        //   // subtitle: Text('Music by Julie Gable. Lyrics by Sidney Stein.'),
                        // ),
                        ifs = (report.kode == null)
                            ? Text("Kode         : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 3.12,
                                ))
                            : Text("Kode              : ${report.kode}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 3.12,
                                )),
                        ifs = (report.kdLokasi == null)
                            ? Text("Kode Lokasi         : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.84,
                                ))
                            : Text("Kode Lokasi       : ${report.kdLokasi}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.84,
                                )),
                        // Text("Kode Barang       : ${report.kdBarang}", style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
                        ifs = (report.urBaru == null)
                            ? Text("Nama Barang         : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 0.7,
                                ))
                            : Text("Nama Barang        : ${report.urBaru}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 0.7,
                                )),
                        ifs = (report.tglPerlh == null)
                            ? Text("Tanggal Perolehan         : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.0,
                                ))
                            : Text("Tanggal Perolehan : ${report.tglPerlh}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.0,
                                )),
                        ifs = (report.merkType == null)
                            ? Text("Merk/Type         : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.7,
                                ))
                            : Text("Merk/Type         : ${report.merkType}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.7,
                                )),
                        ifs = (report.asalPerlh == null)
                            ? Text("Asal            : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 3.30,
                                ))
                            : Text("Asal              : ${report.asalPerlh}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 3.30,
                                )),
                        ifs = (report.rphSat == null)
                            ? Text("Rph Satuan         : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.50,
                                ))
                            : Text("Rph Satuan        : ${report.rphSat}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.50,
                                )),
                        ifs = (report.keterangan == null)
                            ? Text("Keterangan         : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.90,
                                ))
                            : Text("Keterangan        : ${report.keterangan}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.90,
                                )),
                        ifs = (report.noMesin == null)
                            ? Text("No. Mesin         : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.70,
                                ))
                            : Text("No. Mesin         : ${report.noMesin}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.70,
                                )),
                        ifs = (report.noRangka == null)
                            ? Text("No. Rangka        : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.50,
                                ))
                            : Text("No. rangka        : ${report.noRangka}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 2.50,
                                )),
                        ifs = (report.noPolisi == null)
                            ? Text("No. Polisi        : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 3.78,
                                ))
                            : Text("No. Polisi        : ${report.noPolisi}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 3.78,
                                )),
                        ifs = (report.noBpkb == null)
                            ? Text("No. BPKB        : -",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 3.80,
                                ))
                            : Text("No. BPKB          : ${report.noBpkb}",
                                style: TextStyle(
                                  fontSize: 14,
                                  wordSpacing: 3.80,
                                ))
                      ],
                    ),
                    // ),
                  ),
                ),
        ),
      ),
    );
  }
}
