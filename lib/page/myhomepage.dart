import 'package:flutter/material.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:sar_label/page/detail.dart';
//import 'dart:async';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  //String code="";
  //String getcode="";
  // String barcode = '';

  Future scan() async {
    //String barcode = await scanner.scan();
    await scanner.scan()
    .then((String kode){
      Navigator.push(context,MaterialPageRoute(builder: (context) => Detail(kode)));
    });
    //setState(() => this.barcode = barcode);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      appBar: AppBar(title: Text("Aset Scanner Basarnas"),backgroundColor: Colors.orange[300],),
      body: Center(
        child: Column(
          children: <Widget>[
              Image.asset("img/sar.jpg", width: 400.0,),
          ],
        ),
        // child: Column(
        //   children: <Widget>[
        //     FlatButton(
        //       child: Text('Scan Bracode', style: TextStyle(fontSize: 20, color: Colors.black),),
        //       color: Colors.green,
        //       onPressed: (){scan();},
        //     ),
        //   ],
        // ),
      ),
      floatingActionButton: FloatingActionButton.extended(
              icon: Icon(Icons.scanner),
              onPressed: () {
                scan(); // calling a function when user click on button
              },
              backgroundColor: Colors.green,
              label: Text("Scan QR")),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}